
// metodos Antiguos fueron reemplazados por el Query selector
console.log(document.getElementsByTagName("li"));
console.log(document.getElementsByClassName("card"));
console.log(document.getElementsByName("nombre"));

// Ejemplo con Query Selector
console.log(document.querySelector("#menu"));

// Individual
console.log(document.querySelector("a"));
// Traer todos
console.log(document.querySelectorAll("a"));
// Obtiene cantidad
console.log(document.querySelectorAll("a").length);
//Recorre e imprime
document.querySelectorAll("a").forEach((el)=>console.log(el));
//Obtener elemento mediante su clase
console.log(document.querySelector(".card"));
//Obtener la tarjeta numero 3
console.log(document.querySelector(".card")[2]);
// Traer selectores con filtro ( trae solo los que encuentre dentro de nodo menu)
console.log(document.querySelector("#menu li"));


console.log(document.getElementById("menu"));

