function  flujoEventos(e){
    console.log(`Hola te saluda ${this}, el click lo originó ${e.target.className}`);
}

document.addEventListener("click",(e)=>{
    console.log("Click en", e.target);

    // Si el elemento que origina el evento esta dentro de div de eventos-flujo entonces:
    if(e.target.matches(".eventos-flujo div")){
        flujoEventos(e);
    }

    if (e.target.matches(".eventos-flujo a")) {
        alert("Jean Paul Salazar Yañez");
        e.preventDefault();
    } 
})
