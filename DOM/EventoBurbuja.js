const $divsEventos = document.querySelectorAll(".eventos-flujo div");

function  flujoEventos(e){
    console.log(`Hola te saluda ${this.className}, el click lo originó ${e.target.className}`);
}

$divsEventos.forEach((div)=>{
    // fase de burbuja del mas interno al mas externo
    //div.addEventListener("click",flujoEventos,false);
    // fase de captura del mas externo al mas interno
    //div.addEventListener("click",flujoEventos,true);
    //Ejecuta solo una vez el evento y al ser captura en falso se ejecuta del mas interno al externo
    div.addEventListener("click",flujoEventos,{
        capture: false,
        once:true,
    })


})