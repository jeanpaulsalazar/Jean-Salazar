console.log("***********Objeto URL (Location)***********");
console.log(location)
console.log(location.origin)
console.log(location.protocol)
console.log(location.host)
console.log(location.href)
console.log(location.hash)
console.log(location.search)

console.log(location.pathname)

// Historial de navegacion avanzar y retroceder
console.log("***********(History)***********");
console.log(history);
// Cuantas paginas hemos visitado antes
console.log(history.length);
// Nos devuelve en 1 pagina
history.forward(1);
// Avanza en X paginas
history.go(1);

console.log("***********(Objeto Navegador)***********");
console.log(navigator);
console.log(navigator.connection);
console.log(navigator.geolocation);
console.log(navigator.mediaDevices);
console.log(navigator.mimeTypes);
console.log(navigator.onLine);
console.log(navigator.serviceWorker);
console.log(navigator.storage);
console.log(navigator.usb);
console.log(navigator.userAgent);