// Creando elemento
const $figure           = document.createElement("figure");
const $img              = document.createElement("img");
const $figcaption       = document.createElement("figcaption");
const $figcaptiontext   = document.createTextNode("Animals");
const $cards            = document.querySelector(".cards");

// agregando atributos a elementos

$img.setAttribute("src","https://placeimg.com/200/200/animals");
$img.setAttribute("alt","Animals");

// Agregando clase a atributo

$figure.classList.add("card");

// Agregamos los elementos creados al html
$figcaption.appendChild($figcaptiontext);
$figure.appendChild($img);
$figure.appendChild($figcaption);
$cards.appendChild($figure);

// Creando elementos con valores almacenados en un arreglo

const   estaciones =["Primavera","Verano","Otoño","Invierno"],
        $ul = document.createElement("ul");

document.write("<h3>Estaciones del Año</h3>");
document.body.appendChild($ul);

// Recorremos el arreglo con for each

estaciones.forEach((el)=>{
    const $li = document.createElement("li");
    $li.textContent = el;
    $ul.appendChild($li);
});

// La mejor forma de crear elementos es mediante fragmento y una sola insercion al dom como el siguiente ejemplo
/**
 * DocumentFragment son Nodos del DOM que nunca forman parte del arbol DOM. 
 * El caso de uso mas comun es crear un document fragment, 
 * agregar elementos al document fragment y luego agregar dicho document fragment al arbol del DOM. 
 * En el arbol del DOM, el document fragment es remplazado por todos sus hijos.
 * Dado que el document fragment es generado en memoria y no como parte del arbol del DOM, 
 * agregar elementos al mismo no causan reflow (computo de la posicion y geometria de los elementos) en la pagina. 
 * Como consecuencia, usar document fragments usualmente resultan en mejor performance.
 */

const meses = [
    "Enero",
    "Febrero",
    "Marzo",
    "Abril",
    "Mayo",
    "Junio",
    "Julio",
    "Agosto",
    "Septiembre",
    "Octubre",
    "Noviembre",
    "Diciembre"
],
$ul3 = document.createElement("ul"),
$fragment = document.createDocumentFragment();

meses.forEach((el)=>{
    const $li  = document.createElement("li");
    $li.textContent=el;
    $fragment.appendChild($li);
});

document.write("<h3>Meses del Año</h3>");
$ul3.appendChild($fragment);
document.body.appendChild($ul3);