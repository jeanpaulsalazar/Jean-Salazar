// para pasar parametros a un evento se realiza mediante una arrow function

function saludar(nombre ="Desconocido"){
    alert(`Hola ${nombre}`);
}

const  $eventoMultiple = document.getElementById("evento-multiple"),
       $eventoRemover = document.getElementById("evento-remover");

$eventoMultiple.addEventListener("click",()=>{
    saludar();
    saludar("Jean");
})

const removerDobleClick =(e)=>{
    alert(`Removiendo el evento de tipo ${e.type}`);
    console.log(e);
    $eventoRemover.removeEventListener("dblclick",removerDobleClick);
    $eventoRemover.disabled = true;
}

$eventoRemover.addEventListener("dblclick",removerDobleClick);