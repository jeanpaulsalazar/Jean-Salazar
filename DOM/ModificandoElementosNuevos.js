/**
 * <script>
    function myFunction() {
    var x = document.activeElement.value;
    document.getElementById("demo").innerHTML = x;
}
 * 
 * posición
    Un DOMString representando la posición relativa al elementoObjetivo; debe ser una de las siguientes cadenas:

        'beforebegin': Antes del elementoObjetivo.
        'afterbegin': Dentro del elementoObjetivo, antes de su primer hijo.
        'beforeend': Dentro del elementoObjetivo, después de su último hijo.
        'afterend': Después del elementoObjetivo.


 * 
 * InsertAdjacent:
 *      InsertAdjacentElement():inserta un elemento nodo dado en una posición dada con respecto al elemento sobre el que se invoca.
 *      InsertAdjacentHTML(): analiza la cadena de texto introducida como cadena HTML o XML e inserta al árbol DOM los nodos resultantes de dicho análisis en la posición especificada. 
 *      InsertAdjacentText(): Inserta texto
 * 
 * 
 * 
 */

 const $cards = document.querySelector(".cards"),
    $newCard = document.createElement("figure");

$newCard.innerHTML=`
<img src="https://placeimg.com/200/200/any" alt="Any">
<figcaption>Any</figcaption>
`;
$newCard.classList.add("card");
$cards.insertAdjacentElement("beforebegin",$newCard);