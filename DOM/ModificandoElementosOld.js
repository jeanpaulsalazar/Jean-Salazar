
// Metodo antiguo para reemplazar elemento.
const $cards = document.querySelector(".cards"),
$newCard = document.createElement("fugure"),
// Clona todo un nodo
$cloneCards = $cards.cloneNode(true);

$newCard.innerHTML=`
<img src="https://placeimg.com/200/200/any" alt="Any">
<figcaption>Any</figcaption>
`;
$newCard.classList("card");
$cards.replaceChild($newCard,$cards.children[2]);

// Metodo para insertarla antes de un nodo
$cards.insertBefore($newCard,$cards.firstElementChild);

// Metodo para eliminar
$cards.removeChild($cards.lastElementChild);

// clonar
document.body.appendChild($cloneCards);