function holaMundo(){
    alert("Hola Mundo");
    console.log(event);
}

// Evento semantico
const $eventoSemantico = document.getElementById("evento-semantico"),
    $eventoMultiple = document.getElementById("evento-multiple"),

    //Practica de conocimiento
    $ejemplo = document.getElementById("CajaDeTexto"),
    $aviso = document.createElement("p");

    $aviso.innerText = `El campo debe contener solo letras`;
    // Fin de practica
    
$eventoSemantico.onclick = holaMundo;
// con funcion anonima
$eventoSemantico.onclick = function(e){
    alert("Hola mundo con Manejador de Eventos Semantico");
    console.log(e);
    console.log(event);
}
// Eventos multiples

$eventoMultiple.addEventListener("click",holaMundo);
$eventoMultiple.addEventListener("click",(e)=>{
    alert("Hola Mundo Manejador de Eventos Múltiple");
    console.log(e);
    console.log(e.type);
    console.log(e.target);
    console.log(event);
});

// Ejemplo
$ejemplo.addEventListener("change",()=>{
    console.log($ejemplo.value);
    soloLetras();
})

// Validaciones del patterns texto
// Creamos el pattern
const pattern = new RegExp('^[A-Z]+$', 'i');

function soloLetras(){
    console.log(pattern.test($ejemplo.value))
    if(!pattern.test($ejemplo.value)){
        alert("El campo debe contener solo letras");
        console.log($ejemplo.focus());
        $ejemplo.insertAdjacentElement("afterend",$aviso);
    }
};
