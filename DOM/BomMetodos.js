const $btnabrir = document.getElementById("abrir-ventana"),
$btnCerrar = document.getElementById("cerrar-ventana"),
$btnImprimir = document.getElementById("imprimir-ventana");

let ventana;

$btnabrir.addEventListener("click",(e)=>{
    ventana = window.open("https://www.google.com");
});

$btnCerrar.addEventListener("click",(e)=>{
    ventana.close();
});

$btnImprimir.addEventListener("click",(e)=>{
    window.print();
});