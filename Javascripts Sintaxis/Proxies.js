/**
 * Ejemplo y explicacion de los proxy
 * https://dev.to/manufosela/javascript-proxy-algunos-usos-practicos-1hie
 */


const target = {
    notProxied: "original value",
    proxied: "original value"
  };
  
  const handler = {
    get: function(target, prop, receiver) {
      if (prop === "proxied") {
        return "replaced value";
      }
      return Reflect.get(...arguments);
    }
  };
  
  const proxy = new Proxy(target, handler);
  
  console.log(proxy.notProxied); // "original value"
  console.log(proxy.proxied);    // "replaced value"