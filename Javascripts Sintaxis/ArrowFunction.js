// Sin parametros
const saludar = () => console.log('Hola');

saludar();

// Recibiendo un Parametro

const hello = nombre =>console.log(`Hola ${nombre}`);
hello('Jean');

// Retornando un valor

const sumarArrow  = (a,b) => a+b;

console.log(sumarArrow(9,9));

// Más de Una linea de arrow function

const  funcionDeVariasLineas = () =>  {
    console.log("Uno");
    console.log("Dos");
    console.log("Tres");
}

funcionDeVariasLineas();

// ForEach (Recorrer un arreglo en javascript) con arrow function 

const numeros  = [1,2,3,4,5,6];

numeros.forEach((el,index)=>{
    console.log(`${el} esta en la posición ${index}`);
});