let fecha = new Date();
console.log(fecha);

// Obtener mes  dia del mes
console.log(fecha.getDate());
// Obtener día  (D,L,M,M,J.V,S)->0,1,2,3,4,5,6
console.log(fecha.getDay());
// Obtenemos el mes (enero,febrero,marzo,abril,mayo,junio,julio,agosto,septiembre,octubre,noviembre y Diciembre)->0,1,2,3,4,5,6,7,8,9,10,11
console.log(fecha.getMonth());
// Obtenemos el año
console.log(fecha.getFullYear());
// Obtenemos la hora
console.log(fecha.getHours());
// Obtenemos los minutos
console.log(fecha.getMinutes());
// Obtenemos los segundos
console.log(fecha.getSeconds());
// Obtenemos la fecha
console.log(fecha.toLocaleDateString());
//Obtenemos la hora minuto y segundo
console.log(fecha.toLocaleTimeString());
// Libreria para fechas y horas https://momentjs.com/
