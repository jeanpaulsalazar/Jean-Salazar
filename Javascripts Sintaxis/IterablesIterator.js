/*
Iterable: son elementos que su contenido se puede recorrer

En cualquier situación donde se necesita iterar un objeto (por ejemplo al inicio de un bucle for..of), 
su método @@iterator es invocado sin argumentos, y el iterador regresado es utilizador para obtener los valores a iterar.

Algunos tipos integrados cuentan con un comportamiento de iterar por defecto, 
mientras que otros tipos (como Object) no. Los tipos integrados con un método @@iterator son:

    Array.prototype[@@iterator]()
    TypedArray.prototype[@@iterator]()
    String.prototype[@@iterator]()
    Map.prototype[@@iterator]()
    Set.prototype[@@iterator]()

*/

const iterable = [1,2,3,4,5,6]

const iterador = iterable[Symbol.iterator]();

console.log(iterable);
console.log(iterador);

// Para recorrer un iterador

let  next = iterador.next();

while(!next.done){
    console.log(next.value);
    next = iterador.next();
}
