//Las callbacks son funciones que se van a ejecutar despues que otra lo haga

function cuadradoCallback(valor,callback){
    setTimeout(() => {
        callback(valor,valor*valor);
    }, 0|Math.random()*1000);
}

cuadradoCallback(0,(valor,result)=>{
    console.log("Inicia Callback");
    console.log(`Callback:${valor},${result}`);
    cuadradoCallback(1,(valor,result)=>{
        console.log("Inicia Callback");
        console.log(`Callback:${valor},${result}`);
        cuadradoCallback(2,(valor,result)=>{
            console.log("Inicia Callback");
            console.log(`Callback:${valor},${result}`);
            cuadradoCallback(3,(valor,result)=>{
                console.log("Inicia Callback");
                console.log(`Callback:${valor},${result}`);
                cuadradoCallback(4,(valor,result)=>{
                    console.log("Inicia Callback");
                    console.log(`Callback:${valor},${result}`);
                });
            });
        });
    });
});

