// Cortocircuito OR - Cuando el valor de la izquierda en la expresion siempre pueda validar a true, es el valor que se cargara por defecto
function saludar(nombre){
    nombre= nombre||"Desconocido";
    console.log(`Hola ${nombre}`);
}

saludar('Jean');
saludar();

//  Mostrara  el valor verdadero(Cadena)
console.log('Cadena'||'Valor de la derecha');
// Mostrara el valor de la derecha
console.log(false,'Valor de la derecha');



// Cortocircuito AND - cuando el valor de la izquierda en la expresion siempre pueda validar a false, es el valor que se cargara por defecto.

// Mostrara el valor de la Izquierda
console.log(false && 'Valor de la derecha');
