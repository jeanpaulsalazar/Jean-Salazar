console.log('Un registro de lo que ha pasadi en nuestra aplicación');
console.error('Esto es un error');
console.warn('Esto es un aviso');
console.info('Esto es un mensaje informativo');
// limpia la consola
console.clear();
// Grupo en consola
console.group('Curso de Jean Salazar');
console.log('Java');
console.log('Javascripts');
console.log('React native')
console.groupEnd

// Tabla en consola
// Ordena alfabeticamente  (sort)
console.table(Object.entries(console).sort());

// Obtener duracion de tiempo
console.time('Cuanto tiempo tarda mi codigo');
const arreglo = Array(1000000);

for(let i=0;  i< arreglo.length;i++){
    arreglo[i] = i;
}

console.timeEnd('Cuanto tiempo tarda mi codigo');

// contar las veces de ejecución

for(let i=0  ;  i<=100; i++){
    console.count('Codigo for');
    console.log(i);
}

// Enviar mensaje cuando falla una prueba

let  x = 3,
    y = 2,
    pruebaXY = 'Se espera que X siempre sea menor que Y';
console.assert(x<y,{x,y,pruebaXY});