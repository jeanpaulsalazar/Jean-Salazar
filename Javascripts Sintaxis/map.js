/*
Un objeto Map puede iterar sobre sus elementos en orden de inserción. 
Un bucle for..of devolverá un array de [clave, valor] en cada iteración.

Cabe destacar que un Map el cual es un mapa de un objeto, especialmente un diccionario de diccionarios, 
solo se va a mapear en el orden de inserción del objeto — el cual es aleatorio y no ordenado.
*/

let mapa = new Map();

// Para agregar valores se usa el metodo set

mapa.set("nombre","Jean");
mapa.set("Apellido","Salazar");
mapa.set("edad",33);
console.log(mapa);

// el tamaño de map

console.log(mapa.size);

// Para validar si existe una clave o no se ocupa el metodo has

console.log(mapa.has("correo"));
console.log(mapa.has("nombre"));

// para obterner los valores de un map se utiliza el metodo get

console.log(mapa.get("nombre"));


// Para asignar o actualizar un valor del map se utiliza el metodo set

mapa.set("nombre","Jean Paul Salazar")

console.log(mapa.get("nombre"));

// Para eliminar elementos de un map se utiliza el metodo delete

mapa.delete("Apellido");
console.log(mapa);

// Podemos recorrer el map con for of y en el ejemplo usamos destructuracion()

for(let[key,value]of mapa){
    console.log (`Llave:  ${key},Valor: ${value}`);
}



























