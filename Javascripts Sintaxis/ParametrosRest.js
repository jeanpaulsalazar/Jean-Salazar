// Los parametros rest son una forma de virtualmente ir agregando parametros infinitos a una funcion o dentro de una variable

// Ejemplo

function sumar(a,b, ...c){
    let resultado = a+b;

    c.forEach(function(n){
        resultado += n
    });

    return resultado;
};

console.log(sumar(1,2));
console.log(sumar(1,2,3));
console.log(sumar(1,2,3,4,5,6,7,8,9));


