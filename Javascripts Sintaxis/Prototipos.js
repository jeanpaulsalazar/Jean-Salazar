// Es el mecanismo por el cual un objeto podra heredar de un objeto padre atributos y metodos 

// Funcion costructora donde asignamos los metodos
// prototipo no a la funcion como tal (Nos permite ahorrar memoria)

function Animal(nombre,genero){
    //Atributo
    this.nombre = nombre;
    this.genero = genero;  
}
    // Metodo
Animal.prototype.sonar = function(){
    console.log("Hago sonidos");    
}

const   snoopy  =   new Animal('Snoopy','Macho'),
        lola    =   new Animal('Lola Bunny', 'Hembra');

console.log(snoopy);
console.log(lola);

snoopy.sonar();